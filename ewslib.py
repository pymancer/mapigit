#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-11-29

@author: pymancer

exchangelib extensions and helpers
"""
from xml.etree.ElementTree import ParseError

from exchangelib.transport import wrap, MNS, TNS
from exchangelib.version import Build
from exchangelib.services import EWSAccountService
from exchangelib.util import create_element, add_xml_child, to_xml, ElementType
from exchangelib.errors import SOAPError, TransportError

EXCHANGE2010_SP1 = Build(14, 1)


class SubscribeStream(EWSAccountService):
    """
    MSDN: https://msdn.microsoft.com/EN-US/library/office/dn458792(v=exchg.150).aspx

    TODO: add Exchange server affinity support
    """
    SERVICE_NAME = 'Subscribe'
    element_container_name = '{%s}SubscriptionId' % MNS # single Element

    def call(self, folders, event_types):
        """ Subscribe for specified events notifications.

            :param folders: a list of folder predefined names for event tracking
            :param event_types: a list of event type names to subscribe
            :return: Exception or SubscriptionId
            
            example: SubscribeStream(account).call(['inbox'], ['NewMailEvent'])
        """
        if self.protocol.version.build < EXCHANGE2010_SP1:
            raise NotImplementedError('%s is only supported for Exchange 2010 SP1+' % self.SERVICE_NAME)
        elements = self._get_elements(payload=self.get_payload(folders, event_types))

        return list(elements)[0].text

    def get_payload(self, folders, event_types):
        payload = create_element('m:%s' % self.SERVICE_NAME)
        streaming = create_element('m:StreamingSubscriptionRequest')
        
        folderids = create_element('t:FolderIds')
        for folder in folders:
            tag = create_element('t:DistinguishedFolderId', Id=folder)
            folderids.append(tag)
        streaming.append(folderids)

        eventtypes = create_element('t:EventTypes')
        for event_type in event_types:
            add_xml_child(eventtypes, 't:EventType', event_type)
        streaming.append(eventtypes)

        payload.append(streaming)
        return payload

    def _get_elements_in_container(self, container):
        """ Not a container actually, just a single Element
            containing the new subscription's id wrapped
            into list for consistency.
        """
        return [container]


class UnsubscribeStream(EWSAccountService):
    """
    MSDN: https://msdn.microsoft.com/en-us/library/office/aa564263(v=exchg.150).aspx

    TODO: add Exchange server affinity support
    """
    SERVICE_NAME = 'Unsubscribe'
    element_container_name = None

    def call(self, subscription_id):
        """ Unsubscribe from existing events notifications.

            :param subscription_id: target subscription id
            :return: Exception or True
            
            example: UnsubscribeStream(account).call('wtMi5kaW')
        """
        if self.protocol.version.build < EXCHANGE2010_SP1:
            raise NotImplementedError('%s is only supported for Exchange 2010 SP1+' % self.SERVICE_NAME)
        elements = self._get_elements(payload=self.get_payload(subscription_id))

        return list(elements)[0]

    def get_payload(self, subscription_id):
        payload = create_element('m:%s' % self.SERVICE_NAME)
        add_xml_child(payload, 'm:SubscriptionId', subscription_id)
        return payload


class GetStreamingEvents(EWSAccountService):
    """
    MSDN: https://msdn.microsoft.com/en-us/library/office/dn458792(v=exchg.150).aspx#Anchor_2
    Not working: https://msdn.microsoft.com/EN-US/library/office/ff406172(v=exchg.150).aspx

    TODO: add Exchange server affinity support
    """
    SERVICE_NAME = 'GetStreamingEvents'
    element_container_name = '{%s}Notifications' % MNS

    def call(self, subscription_ids, timeout):
        """ Get subscribed events for specified time.
            Could be called multiple times for 30 minutes.
            On timeout server responses by Success NoError message
            with ConnectionStatus>OK</ns0:ConnectionStatus> tag.

            :param subscription_id: target subscription id
            :param timeout: stream timeout in minutes (1-30)
            :param callback: function taking a single argument
                   containing a chunk of received data
            :return: Exception (timeout or otherwise) or True.
            example: GetStreamingEvents(account).call('wtMi5kaW', 1)
        """
        if self.protocol.version.build < EXCHANGE2010_SP1:
            raise NotImplementedError('%s is only supported for Exchange 2010 SP1+' % self.SERVICE_NAME)
        # TODO: add persistent connection error handling
        # subscription should single, payload only follows formats
        assert len(subscription_ids) == 1, 'multiple subscriptions detected'
        payload=self.get_payload(subscription_ids, timeout)
        account = self.account
        session = self.protocol.get_session()
        soap_payload = wrap(content=payload, version=self.protocol.version.api_version, account=account)

        r = session.post(url=self.protocol.service_endpoint,
                         headers=None,
                         data=soap_payload,
                         allow_redirects=False,
                         timeout=self.protocol.TIMEOUT,
                         stream=True)

        for chunk in r.iter_content(chunk_size=1024*1024):
            # filter out keepalive chunks
            # TODO: handle zero-length byte instead of just swallowing it with `strip`
            if chunk and chunk.decode('utf-8').strip():
                try:
                    soap_response_payload = to_xml(chunk.decode('utf-8'))
                except ParseError as e:
                    raise SOAPError('Bad SOAP response: %s' % e)
                soap_payload = self._get_soap_payload(soap_response=soap_response_payload)
                try:
                    elements = self._get_elements_in_response(response=soap_payload)
                    for element in elements:
                        for message in self.get_messages(element):
                            # each 'message' contains message id and changekey
                            yield (message)
                except TransportError:
                    # TODO: implement full soap_payload processing in one step
                    # mimicking self._get_elements_in_response behaviour
                    assert isinstance(soap_payload, list)
                    for message_or_exc in soap_payload:
                        assert isinstance(message_or_exc, ElementType)
                        tag = '{%s}ConnectionStatus' % MNS
                        container_or_exc = self._get_element_container(message=message_or_exc, name=tag)
                        if isinstance(container_or_exc, ElementType):
                            if container_or_exc.text == 'OK':
                                # connection is ok
                                pass
                            elif container_or_exc.text == 'Closed':
                                # iterator stop
                                return container_or_exc.text
                            else:
                                # unexpected behaviour
                                raise TransportError(container_or_exc.text)
        else:
            self.protocol.release_session(session)

    def get_payload(self, subscription_ids, timeout):
        payload = create_element('m:%s' % self.SERVICE_NAME)
        subscriptions = create_element('m:SubscriptionIds')
        for subscription_id in subscription_ids:
            add_xml_child(subscriptions, 't:SubscriptionId', subscription_id)
        payload.append(subscriptions)
        add_xml_child(payload, 'm:ConnectionTimeout', timeout)
        return payload

    def get_messages(self, notification):
        """ Extracts new message id(s) from xml element. """
        new_mail_events = notification.findall('{%s}NewMailEvent' % TNS)
        messages = []
        for new_mail_event in new_mail_events:
            message_id = new_mail_event.find('{%s}ItemId' % TNS).attrib['Id']
            change_key = new_mail_event.find('{%s}ItemId' % TNS).attrib['ChangeKey']
            messages.append((message_id, change_key))
        return messages

