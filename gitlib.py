#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-03

@author: pymancer

GitPython extensions and helpers
"""
import os.path

from abc import ABC, abstractmethod

from git import Repo
from git.exc import NoSuchPathError, InvalidGitRepositoryError, GitCommandError

from logger import Log
from config import CFG
from helpers import get_main_name, get_tmp, get_files, to_nix_path


class AbstractNode(ABC):
    __instance = None
    _bundle_re = r'^[0-9a-f]{32}\.bundle'

    def __new__(cls, *args, **kwargs):
        if CLIENT.__instance is None:
            CLIENT.__instance = object.__new__(cls)
        return CLIENT.__instance

    def __init__(self, repo_path=None, tmp_path=None):
        self.log = Log(self.__class__.__name__.lower()).get()
        self.log.info('Git {} executor init'.format(get_main_name()))
        cfg = CFG()
        self.repo_path = repo_path or cfg.get_repo_path()
        self.tmp = tmp_path or cfg.get_tmp_path()
        try:
            self.repo = Repo(self.repo_path)
            # bases cleanup
            branches = [head.name for head in self.repo.heads]
            bases = cfg.get_bases()
            for base in bases:
                if base not in branches:
                    cfg.set_tag(base)
        except (NoSuchPathError, InvalidGitRepositoryError):
            self.log.debug('repo path is not a Repo')
            self.repo = None

    @abstractmethod
    def apply_bundle(self, tag, args=None):
        pass

    def _set_repo_parameters(self):
        cfg = CFG()
        repo_user_name = cfg.conf.get('Repo', 'UserName', fallback='')
        repo_user_email = cfg.conf.get('Repo', 'UserEmail', fallback='')
        with self.repo.config_writer() as cw:
            if repo_user_name:
                cw.set_value('user', 'name', repo_user_name)
            if repo_user_email:
                cw.set_value('user', 'email', repo_user_email)

    def _create_bundle(self, tag, args=None, is_reply=False):
        """ Creates bundle of target branch starting from the existing ref
            param: tag: message batch tag a.k.a target folder and bundle name,
            param: is_reply: create bundle in 'reply' folder
            TODO: implement multiple branch support e.g. with {branch: start_tag} file,
                  for now developer could use local branches but apply all
                  remote operations on single branch, e.g. 'develop'
        """
        # always use saved bundle start point or pack the whole branch
        start_tag = CFG().get_tag(self.get_active_branch())
        bundle_path = os.path.join(get_tmp(self.tmp, tag, is_reply=is_reply, create=True), '{}.bundle'.format(tag))
        ref = '{}..{}'.format(start_tag, self.get_active_branch()) if start_tag else self.get_active_branch()

        refs = self._get_refs()

        try:
            created = self.repo.git.bundle('create', bundle_path, ref)
        except GitCommandError as e:
            return (False,  {'cmd': 'error', 'args': 'GTILIB-e1: {}'.format(e), 'refs': refs})

        msg = 'BUNDLE[{} created in {}]: {}'.format(ref, bundle_path, created)
        return (True, {'cmd': 'success', 'args': msg, 'refs': refs})

    def get_active_branch(self):
        if self.repo:
            return self.repo.active_branch.name
        else:
            return ''

    def set_tag(self, tag, replace=False, push=False, conf=False):
        """ Tag current tip of the branch.
            param: tag: tag name,
            param: replace: create tag even if it's name already in use,
            param: push: push tag to remote
            param: conf: update tag in the configuration file
        """
        if self.repo:
            if replace:
                try: 
                    deleted = self.repo.delete_tag(tag)
                    self.log.debug('TAG[DELETE]: {}'.format(deleted))
                except GitCommandError as e:
                    self.log.debug('NO TAG[DELETE]: {}, Error: {}'.format(tag, e))
            try: 
                tagged = self.repo.create_tag(tag)
                self.log.debug('TAG[CREATE]: {}'.format(tagged))
            except GitCommandError as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e2: {}'.format(e)})
            else:
                if push:
                    try: 
                        pushed = self.repo.git.push('--tags')
                        self.log.debug('PUSH[TAG]: {}'.format(pushed))
                    except GitCommandError as e:
                        return (False, {'cmd': 'error', 'args': 'GTILIB-e3: {}'.format(e)})
                if conf:
                    # write tag to config to use as bundle start point
                    CFG().set_tag(self.get_active_branch(), tag)
                return (True, {'cmd': 'success', 'args': 'Tag {} set'.format(tag)})
        return (True, {'cmd': 'success', 'args': 'No repo to tag'})

    def _get_refs(self):
        """ Collects and returns existing references as a dictionary: {branch: ref} """
        refs = {}
        for head in self.repo.heads:
            refs[head.name] = head.commit.hexsha
        return refs

    def _set_refs(self, refs):
        """ Updates existing references from dictionary: {branch: ref} """
        if refs:
            for head in self.repo.heads:
                try:
                    if refs.get(head.name, None):
                        head.set_reference(refs[head.name])
                except GitCommandError as e:
                    self.log.warning('REFERENCE: {}, Error: {}'.format(head.name, e))


class CLIENT(AbstractNode):
    """ Git commands wrapper for mapigit client side operations. Singleton. """
    def create_bundle(self, tag, args=None):
        """ Creates bundle branch starting from tag.
            param: tag: starting tag
        """
        return self._create_bundle(tag, args=args)

    def apply_bundle(self, tag, args=None):
        """ Applies recieved bundle to the repo.
            param: tag: bundle's starting point tag
            param: args: git command parts and optional refs {'cmd': 'cmd', 'args': args, 'refs': refs}
            returns: result
        """
        bundles = get_files(get_tmp(self.tmp, tag, is_reply=True), self._bundle_re)
        if len(bundles) == 1:
            # shortcut
            origin = self.repo.remotes.origin
            # set target bundle as a new origin
            try:
                with origin.config_writer as cw:
                    cw.set('url', to_nix_path(bundles[0][1])) # file path out of the (filename, filepath) tuple
            except Exception as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e4: {}'.format(e)})
            # pull from the current (merge same branch) or targeted origin (merge another branch into active)
            try:
                pulled = self.repo.git.pull()
                self.log.debug('PULL[APPLY]: {}'.format(pulled))
            except GitCommandError as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e5: {}'.format(e)})
            # extracting sources (not in 'bare' repo)
            if not self.repo.bare:
                try:
                    checked = self.repo.git.checkout()
                    self.log.debug('CHECKOUT[APPLY]: {}'.format(checked))
                except GitCommandError as e:
                    return (False, {'cmd': 'error', 'args': 'GTILIB-e9: {}'.format(e)})
            msg = '{}[SYNC]: {}'.format(args['cmd'].upper() if args else 'APPLY', pulled)
            return (True, {'cmd': 'success', 'args': msg})
        else:
            # updating references just in case
            if args:
                self._set_refs(args.get('refs', None))
            msg = 'Bundle file not found or ambiguous.'.format(tag)
            return (True, {'cmd': 'success', 'args': msg})

    def apply_bundle_bare(self, tag, args=None):
        """ Apply bundle when the target repo is not exists, e.g. on clone
            param: tag: batch tag
            param: args: git command parts and optional refs {'cmd': 'cmd', 'args': args, 'refs': refs}
        """
        bundles = get_files(get_tmp(self.tmp, tag, is_reply=True), self._bundle_re)
        if len(bundles) == 1:
            # create repo from the new origin
            source_path = bundles[0][1]
            try:
                self.repo = Repo.clone_from(source_path, self.repo_path)
                # failsafe check if clone didn't return some fancy response
                assert isinstance(self.repo, Repo)
                self.log.debug('{} cloned to {}'.format(source_path, self.repo_path))
            except (GitCommandError, AssertionError) as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e6: {}'.format(e)})
            # set target branch as active and extract sources from Git base (not in 'bare' repo)
            if not self.repo.bare:
                try:
                    checked = self.repo.git.checkout(args.get('branch', None))
                    self.log.debug('CHECKOUT[CLONE]: {}'.format(checked))
                    # setting the new repo's mandatory parameters
                    self._set_repo_parameters()
                except GitCommandError as e:
                    return (False, {'cmd': 'error', 'args': 'GTILIB-e7: {}'.format(e)})
            return (True, {'cmd': 'success', 'args': 'Repo {} created'.format(self.repo_path)})
        else:
            msg = 'Bundle file not found or ambiguous.'.format(tag)
            return (True, {'cmd': 'success', 'args': msg})

    def status(self, args=None):
        """ Just show the operation result, no special handling """
        try:
            status = self.repo.git.status(args)
        except GitCommandError as e:
            return (False, {'cmd': 'error', 'args': 'GTILIB-e8: {}'.format(e)})
        return (True, {'cmd': 'success', 'args': status})


class SERVER(AbstractNode):
    """ git commands wrapper for mapigit server side operations as singleton """
    def create_bundle(self, tag, args=None):
        """ Creates bundle of target branch starting from tag.
            param: tag: starting tag
            server can reply only
        """
        return self._create_bundle(tag, is_reply=True, args=args)

    def apply_bundle(self, tag, args=None):
        """ Applies bundle to repo.
            param: tag: bundle's starting point tag
        """
        bundles = get_files(get_tmp(self.tmp, tag, is_reply=False), self._bundle_re)
        bundles_num = len(bundles)
        if bundles_num == 1:
            # save old origin
            try:
                with self.repo.remotes.origin.config_reader as cr:
                    origin_url = cr.get('url')
            except Exception as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e10: {}'.format(e)})
            # set target bundle as a new origin
            try:
                bundle_path = to_nix_path(bundles[0][1])
                with self.repo.remotes.origin.config_writer as cw:
                    cw.set('url', bundle_path)
                self.log.debug('origin set to {}'.format(bundle_path))
            except Exception as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e11: {}'.format(e)})
            # pull from new origin
            try:
                pulled = self.repo.git.pull(args)
            except GitCommandError as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e17: {}'.format(e)})
            # load back saved origin
            try:
                with self.repo.remotes.origin.config_writer as cw:
                    cw.set('url', origin_url)
                self.log.debug('origin set to {}'.format(origin_url))
            except Exception as e:
                return (False, {'cmd': 'error', 'args': 'GTILIB-e12: {}'.format(e)})
            # set start tag for the next bundle (moved here to update tag on
            # each applied bundle, but not if there was no bundle to apply)
            tagged, msg = self.set_tag(tag, conf=True)
            if not tagged:
                return (tagged, msg)
            # return `pull` answer explicitly
            return (True, {'cmd': 'success', 'args': 'PULL[APPLY]: {}'.format(pulled)})
        elif bundles_num == 0:
            msg = 'no bundle file, skipping sync'.format(tag)
            self.log.info(msg)
            return (True, {'cmd': 'success', 'args': msg})
        else:
            msg = 'bundle files has not been merged'.format(tag)
            self.log.error(msg)
            return (False, {'cmd': 'error', 'args': msg})

    def clone(self, body):
        # TODO: implement arguments dynamic order handling
        self.log.info('clone operation started.')
        if self.repo:
            msg = 'repository {} already exists.'.format(self.repo_path)
            return (False, {'cmd': 'error', 'args': msg})
        if os.path.exists(self.repo_path):
            msg = 'directory {} exists.'.format(self.repo_path)
            return (False, {'cmd': 'error', 'args': msg})

        cfg = CFG()
        bases = cfg.get_bases()
        if bases:
            # clone should initiate a new synchronization cycle
            cfg.set_tag()
            self.log.warning('bases {} removed from the config file'.format(bases))

        try:
            source_path = body['url']
            branch = body.get('branch', cfg.conf.get('Repo', 'DefaultBranch', fallback='master'))
            self.repo = Repo.clone_from(source_path, self.repo_path, b=branch)
            # failsafe check if clone didn't return some fancy response
            assert isinstance(self.repo, Repo)
            self.log.debug('{} cloned to {}'.format(source_path, self.repo_path))
            # setting the new repo's mandatory parameters
            self._set_repo_parameters()
            return (True, {'cmd': 'success', 'args': 'CLONE: {}'.format(self.repo_path)})
        except GitCommandError as e:
            return (False,  {'cmd': 'error', 'args': 'GTILIB-e13: {}'.format(e)})

    def push(self, body):
        """ by default push to the remote branch with the name of the local active one
            TODO: apply git global config settings instead
        """
        try:
            if body.get('args', None):
                pushed = self.repo.git.push(body['args'])
            else:
                pushed = self.repo.git.push('origin', '{branch}:{branch}'.format(branch=self.get_active_branch()))
        except GitCommandError as e:
            return (False, {'cmd': 'error', 'args': 'GTILIB-e14: {}'.format(e)})
        else:
            return (True, {'cmd': 'success', 'args': 'PUSH: {}'.format(pushed)})

    def pull(self, body):
        """ by default pull the remote branch with the name of the local active one
            TODO: apply git global config settings instead
        """
        try:
            if body.get('args', None):
                pulled = self.repo.git.pull(body['args'])
            else:
                pulled = self.repo.git.pull('origin', self.get_active_branch())
        except GitCommandError as e:
            return (False, {'cmd': 'error', 'args': 'GTILIB-e15: {}'.format(e)})
        else:
            return (True, {'cmd': 'success', 'args': 'PULL: {}'.format(pulled)})

    def fetch(self, body):
        """ by default just fetch `origin`
            TODO: apply git global config settings instead
        """
        try:
            if body.get('args', None):
                fetched = self.repo.git.fetch(body['args'])
            else:
                fetched = self.repo.git.fetch('origin')
        except GitCommandError as e:
            return (False, {'cmd': 'error', 'args': 'GTILIB-e16: {}'.format(e)})
        else:
            return (True, {'cmd': 'success', 'args': 'FETCH: {}'.format(fetched)})

