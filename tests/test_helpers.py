#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-10

@author: pymancer

helpers testing module

example run (from shell):
py.test test_helpers.py
"""
import os.path
import filecmp


def test_files_sort_key():
    from helpers import files_sort_key
    list_to_sort = ['abc.bundle.11', 'abc.bundle.2']
    assert sorted(list_to_sort, key=files_sort_key) ==  ['abc.bundle.2', 'abc.bundle.11']


def test_split_merge():
    from helpers import split_file, merge_files
    # testing on existing files
    # could be generated if necessary
    original = os.path.abspath('./tests/data/abc.bundle')
    backup = os.path.abspath('./tests/data/abc.bundle.bkp')
    folder = os.path.dirname(original)
    assert split_file(folder, max_file_size=2048) == [tuple(i + '.1' for i in (os.path.basename(original), original)),
                                                      tuple(i + '.2' for i in (os.path.basename(original), original)),
                                                      tuple(i + '.3' for i in (os.path.basename(original), original))]
    assert merge_files(folder) == (os.path.basename(original), original)
    assert filecmp.cmp(original, backup)

def test_split_merge_small():
    from helpers import split_file, merge_files
    # testing on existing files
    # could be generated if necessary
    original = os.path.abspath('./tests/data/abc.bundle')
    backup = os.path.abspath('./tests/data/abc.bundle.bkp')
    folder = os.path.dirname(original)
    mfs = 1024 * 1024
    assert split_file(folder, max_file_size=mfs) == [tuple(i + '.1' for i in (os.path.basename(original), original))]
    assert merge_files(folder) == (os.path.basename(original), original)
    assert filecmp.cmp(original, backup)
