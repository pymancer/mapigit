#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-07

@author: pymancer

general utilities
"""
import os
import re
import shutil

import __main__

from uuid import uuid4

SUBJSEP = '::'


def get_main_name():
    try:
        name = os.path.splitext(__main__.__file__)[0]
    except AttributeError:
        # python shell run
        name = 'mapigit'
    
    return name


def get_new_tag():
    return uuid4().hex


def get_subject(prefix, part=1, parts=1, tag=None):
    """ param: part: current batch part, in case multiple attachments
                     in the single message it should be set
                     to the total number of them in that particular message
        param: parts: total parts in batch, in case multiple attachments
                      in the single message it should be set to the
                      total number of the attachments int the batch
        param: tag: batch tag
        returns: a tuple with the tag and the generated subject
    """
    tag = tag or get_new_tag()
    subject = '{}{sep}{}{sep}{}{sep}{}'.format(prefix, tag, part, parts, sep=SUBJSEP)
    return (tag, subject)


def parse_subject(subject):
    """ param: subject: subject as string
        returns: a tuple of 4 items containing (prefix, tag, part, parts)
                 or if the subject has an unknown structure None objects
        # TODO: implement returned tuple as object?
    """
    # by default any subject has an unknown format
    parsed = subject.split(SUBJSEP) 
    if len(parsed) == 4:
        # this message could be for us, checking and coercing
        try:
            assert re.match(r'[0-9a-f]{32}', parsed[1], flags=re.IGNORECASE) # tag as guid
            parsed[2] = int(parsed[2]) # part
            parsed[3] = int(parsed[3]) # parts
        except (ValueError, AssertionError):
            # nothing bad happend, just an unknown message
            pass
        else:
            return parsed

    return (None, None, None, None)


def get_files(folder, pattern=None):
    """ param: folder: full path to the target folder
        param: pattern: only files matching a pattern should be counted
        returns: list of tuples (filename, fullpath)
    """
    files = []
    if os.path.exists(folder):
        for item in os.listdir(folder):
            itempath = get_path(folder, item)
            if not pattern or re.match(pattern, item, flags=re.IGNORECASE):
                files.append((item, itempath))
    return files


def get_tmp(tmp_path, tag, is_reply=False, create=False):
    """ shortcut, secures replies folder name """
    full_path = get_path(tmp_path, tag, 'reply') if is_reply else get_path(tmp_path, tag)
    if create:
        os.makedirs(full_path, exist_ok=True)
    return full_path


def files_sort_key(name):
    """ natural sorting order key function """
    return int(os.path.splitext(name)[1][1:])


def split_file(folder, max_file_size=1024 * 1024 * 4, pattern=r'^[0-9a-f]+\.bundle$'):
    """ Splits a file in the folder to comply with maximum allowed size.
        param: folder: full path to the folder with the target file
        param: max_file_size: splitted files maximum size in bytes
        param: pattern: regexp pattern for the target file's name
        returns: sorted list of tuples [(filename, fullpath)]
        naming example: filename.bundle / max_file_size = filename.bundle.1, filename.bundle.2
        Original file will be deleted after split.
        Only single file in the folder should match the pattern.
    """
    assert max_file_size > 0
    splitted_files = []
    chunk_size = min(max_file_size, 65536)
    if os.path.exists(folder):
        bundle_files = []
        for item in os.listdir(folder):
            item_path = os.path.join(folder, item)
            if os.path.isfile(item_path) and re.match(pattern, item):
                bundle_files.append(item_path)
        if len(bundle_files) == 1:
            original = bundle_files[0]
            original_size = os.path.getsize(original)
            parts = -(-original_size // max_file_size)
            with open(original, mode='rb') as f_in:
                for i in range(1, parts + 1):
                    f_out_size = 0
                    part_name = '{}.{}'.format(original, i)
                    with open(part_name, mode='wb') as f_out:
                        while f_out_size + chunk_size <= max_file_size:
                            chunk = f_in.read(chunk_size)
                            if not chunk:
                                break # while
                                break # for
                            f_out_size += chunk_size
                            f_out.write(chunk)
                    splitted_files.append((os.path.basename(part_name), part_name))
            os.remove(original)
    return splitted_files


def merge_files(folder, pattern=r'^[0-9a-f]+\.bundle.\d+$'):
    """ Merges all files in the target folder into one.
        Will not process nested folders.
        param: folder: full path to the target folder
        param: pattern: regexp pattern for the splitted files names
        returns: tuple of (filename, filepath) or empty one
        naming example: filename.bundle.1 + filename.bundle.2 = filename.bundle
        Splitted files will be deleted after merge.
        Merged file will be saved in the same folder.
    """
    merged = tuple()
    compiled_pattern = re.compile(pattern)
    if os.path.exists(folder):
        files = []
        for item in os.listdir(folder):
            item_path = os.path.join(folder, item)
            if os.path.isfile(item_path) and compiled_pattern.match(item):
                files.append(item_path)
        if files:
            files = sorted(files, key=files_sort_key)
            merged_file = os.path.splitext(files[0])[0]
            with open(merged_file, mode='wb') as f_out:
                for f in files:
                    with open(f, mode='rb') as f_in:
                        shutil.copyfileobj(f_in, f_out)
                    os.remove(f)
            merged = (os.path.basename(merged_file), merged_file)
    return merged


def get_repo_name(url):
    ns = re.search(r'^((git|ssh|http(s)?)|(git@[\w\.]+))(:(\/\/)?)([\w\.@\:/\-~]+)(\.git)(\/)?$', url).group(7)
    try:
        project = ns.split('/')[-1]
    except IndexError:
        project = None
    return project


def get_path(root, *args):
    """ param: beginning part of the target path
        param: args: iterable of the target path parts
        returns: path as args prepended with root
    """
    drive, path = os.path.splitdrive(root)
    path = os.path.join(path, *args)
    return os.path.join(drive, os.sep, path)


def to_nix_path(win_path):
    chars = []
    for i in range(len(win_path)):
        char = win_path[i]
        if char == '\\':
            chars.append('/')
        else:
            chars.append(char)

    return ''.join(chars)

