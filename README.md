### Git over MAPI synchronizer

#### Requirements
Exchange Server 2013+
Python 3.4+
Git 2.15+

#### Installation
pip install -r .\requirements\prod.txt

#### Testing
python -m pytest tests