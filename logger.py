#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-05

@author: pymancer

App Logger (one file per module)
"""
import logging
import os.path

from logging.handlers import RotatingFileHandler

from config import CFG


class Log:
    """ Logger setup """
    def __init__(self, name, level=None):
        conf = CFG().conf
        self._levels = {'WARNING': logging.WARNING, 'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
        log_name = name.replace('.log', '')
        self._logger = logging.getLogger('log_%s' % log_name)
        self._logger.setLevel(level or self._levels[conf.get('Logging', 'Level', fallback='WARNING')])
        if not self._logger.handlers:
            to_file = True if conf.getboolean('Logging', 'ToFile', fallback='True') == 'True' else False
            if to_file:
                log_file = os.path.join(conf.get('Logging', 'Path', fallback='.'), '{}.log'.format(log_name))
                handler = RotatingFileHandler(log_file, mode='a', maxBytes=5*1024*1024,
                                              backupCount=2, encoding=None, delay=0)
            else:
                handler = logging.StreamHandler()
            formatter = logging.Formatter('%(asctime)s - %(threadName)s - %(levelname)s: %(message)s')
            handler.setFormatter(formatter)
            self._logger.addHandler(handler)

    def get(self, level=None):
        if level:
            assert level in self._levels
            self._logger.setLevel(level)
        return self._logger

