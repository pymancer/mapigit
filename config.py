#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-05

@author: pymancer

App configuration loader
"""
import sys
import json

from configparser import ConfigParser

from helpers import get_main_name, get_path


class CFG:
    """ Configuration provider as singleton. """
    __instance = None

    def __new__(cls, *args, **kwargs):
        if CFG.__instance is None:
            CFG.__instance = object.__new__(cls)
        return CFG.__instance

    def __init__(self):
        self._file = '{}.cfg'.format(get_main_name())
        self._conf = ConfigParser(delimiters=(':'))
        self._conf.optionxform=str
        if not self._conf.read(self._file):
            # creating a default configuration if no existing one
            self._conf['Repo'] = {'Path': './repo', # artificial repo name, could be better to leave empty
                                 'Tmp': './tmp',
                                 'UserName': '',
                                 'UserEmail': '',
                                 'DefaultBranch': '',
                                 'Bases': '{}'}
            self._conf['Logging'] = {'Path': '.',
                                     'ToFile': True,
                                     'Level': 'WARNING'}
            self._conf['Attachment'] = {'MaxFileSize': 4 * 1024 * 1024, # ~ 4MB
                                        'MaxFiles': 1}
            self._conf['EWS'] = {'AuthType': 'NTLM',
                                 'Domain': '',
                                 'UserName': '',
                                 'Server': '',
                                 'Version': '',
                                 'SMTPAddress': '',
                                 'ToAddress': '',
                                 'StreamTimeout': 1,
                                 'LocalPrefix': '',
                                 'RemotePrefix': ''}

            with open(self._file, mode='w') as f:
                self._conf.write(f)
            sys.exit('MAPIGit was not configured properly. Please, check the {} file'.format(self._file))

    @property
    def conf(self):
        """ config parser instance """
        return self._conf

    def get_repo_path(self, *args):
        return get_path(self._conf.get('Repo', 'Path', fallback='./repo'), *args)

    def get_tmp_path(self, *args):
        """ `tmp` folder contains an outgoing and incoming bundles """
        return get_path(self._conf.get('Repo', 'Tmp', fallback='./tmp'), *args)

    def get_bases(self):
        """ returns base tags as dictionary {'branch': 'tag'} """
        return json.loads(self._conf.get('Repo', 'Bases', fallback=dict()))

    def get_tag(self, branch):
        """ returns last bundle's tag """
        return self.get_bases().get(branch, None)

    def set_tag(self, branch=None, tag=None):
        """ sets and saves current bundle's tag for the specified branch
            removes all tags (new repo init) if no branch provided
        """
        bases = self.get_bases()
        if branch:
            if tag:
                bases[branch] = tag
            else:
                # there is no point in keeping branch without a baseline
                try:
                    del bases[branch]
                except KeyError:
                    pass
        else:
            assert not tag # tag must be set only with the corresponding branch
            bases = dict()
        self._conf.set('Repo', 'Bases', json.dumps(bases))
        with open(self._file, mode='w') as f:
            self._conf.write(f)

