#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-05

@author: pymancer

Remote to local Git
over EWS synchronizer
"""
import sys
import os.path

from ews import EWS
from gitlib import SERVER
from logger import Log


def log_and_send_error(ews, msg, tag, sender):
    log.error(msg)
    ews.send_message({'cmd': 'error', 'args': 'SERVER: {}'.format(msg)}, tag=tag, to=sender)

if __name__ == '__main__':
    log = Log(os.path.splitext(__file__)[0]).get()
    log.info('Starting server')
    ews = EWS()
    git = SERVER()

    try:
        while True:
            # stream should be restarted after each timeout
            stream = ews.exec_stream()
            # TODO: handle occasional ChunkedEncodingError
            for message in stream:
                received = ews.receive_message(message)
                # get command without arguments
                cmd = received['body']['cmd']
                sender = received['sender']
                tag = received['tag']
                # reply to the client should have the original batch tag
                # reply body structure: ['error|success', 'message']
                if cmd in ('noop', 'partial'):
                    pass
                elif cmd == 'error':
                    log_and_send_error(ews, received['body']['args'], tag, sender)
                else:
                    # apply client's changes if any
                    applied, msg = git.apply_bundle(tag)
                    if applied:
                        # process the message with the corresponding git handler
                        try:
                            # each handler should return a tuple
                            # containing git operation result ('error|success', {'cmd': 'cmd', 'args': msg})
                            executed, msg = getattr(git, cmd)(received['body'])
                        except AttributeError:
                            log_and_send_error(ews, '{} command unrecognized'.format(cmd), tag, sender)
                        else:
                            if executed:
                                # create bundle to send
                                created, msg = git.create_bundle(tag)
                                # send bundle
                                if created:
                                    # saving new bundle's tag to start from next time, hence save to config too
                                    tagged, msg = git.set_tag(tag, replace=True, conf=True)
                                    if tagged:
                                        if ews.send_message(msg, tag=tag, to=sender, is_reply=True):
                                            log.info('Batch {} sent to {}'.format(tag, sender))
                                        else:
                                            msg = 'while sending a new bundle'.format(cmd)
                                            log_and_send_error(ews, msg, tag, sender)
                                    else:
                                        log_and_send_error(ews, msg, tag, sender)
                                else:
                                    log_and_send_error(ews, msg, tag, sender)
                            else:
                                log_and_send_error(ews, msg, tag, sender)
                    else:
                        log_and_send_error(ews, msg, tag, sender)
    except KeyboardInterrupt:
        ews.close()
        sys.exit('Terminated by user')

