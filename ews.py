#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-05

@author: pymancer

EWS connector.
"""
import os
import re
import sys
import json

from getpass import getpass
from base64 import b64encode, b64decode
from binascii import Error as BinAsciiError
from exchangelib import (NTLM, BASIC, DIGEST, DELEGATE,
                         Account, ServiceAccount, Configuration,
                         Mailbox, Message, Build, Version,  FileAttachment)
from exchangelib.services import GetItem
from exchangelib.items import IdOnly
from exchangelib.errors import ErrorSubscriptionNotFound, ErrorItemNotFound

from logger import Log
from config import CFG
from ewslib import SubscribeStream, UnsubscribeStream, GetStreamingEvents
from helpers import (parse_subject, get_files, get_tmp, split_file,
                     merge_files, get_main_name, get_subject, get_new_tag)



class EWS:
    """ EWS connection handling service as singleton. """
    __instance = None
    _connected = False
    cached_endpoint = None
    subscription_id = None

    def __new__(cls, *args, **kwargs):
        if EWS.__instance is None:
            EWS.__instance = object.__new__(cls)
        return EWS.__instance

    def __init__(self):
        self.log = Log(self.__class__.__name__.lower()).get()
        self.log.info('EWS service init')

        mail_regexp =  re.compile(r'[^@\s]+@[^@\s]+\.[^@\s]+', re.IGNORECASE)
        auth_types = {'BASIC': BASIC, 'DIGEST': DIGEST, 'NTLM': NTLM}

        # mandatory configuration parameters
        conf = CFG().conf
        self.domain = conf.get('EWS', 'Domain', fallback='')
        self.username = conf.get('EWS', 'UserName', fallback='')
        self.server = conf.get('EWS', 'Server', fallback='')
        self.smtp_address = conf.get('EWS', 'SMTPAddress', fallback='')
        self.to_address = conf.get('EWS', 'ToAddress', fallback='')
        # artifical prefixes for all mapigit messages, other modules should not know about them
        # should be unique across the local environment and known by the connected counterparts
        self.local_prefix = conf.get('EWS', 'LocalPrefix', fallback='')
        self.remote_prefix = conf.get('EWS', 'RemotePrefix', fallback='')

        config_ok = True
        if self.domain:
            self.log.info('domain: {}'.format(self.domain))
        else:
            config_ok = False
            self.log.error('invalid or undefined domain name')
        if self.username:
            self.log.info('username: {}'.format(self.username))
        else:
            config_ok = False
            self.log.error('invalid or undefined username')
        if self.server:
            self.log.info('server: {}'.format(self.server))
        else:
            config_ok = False
            self.log.error('invalid or undefined server name')
        if mail_regexp.search(self.smtp_address):
            self.log.info('SMTP address: {}'.format(self.smtp_address))
        else:
            config_ok = False
            self.log.error('invalid or undefined smtp address')
        if mail_regexp.search(self.to_address):
            self.log.info('Server address: {}'.format(self.to_address))
        else:
            config_ok = False
            self.log.error('invalid or undefined server address')
        if self.local_prefix:
            self.log.info('self prefix: {}'.format(self.local_prefix))
        else:
            config_ok = False
            self.log.error('invalid or undefined self prefix')
        if self.remote_prefix:
            self.log.info('remote prefix: {}'.format(self.remote_prefix))
        else:
            config_ok = False
            self.log.error('invalid or undefined remote prefix')

        if not config_ok:
            self.log.error('Check {}.cfg and password, please. Exiting.'.format(get_main_name()))

            sys.exit('Abnormal termination. Check {}.log for errors'.format(self.log.name.replace('log_', '')))

        # optional configuration parameters
        self.auth_type = auth_types[conf.get('EWS', 'AuthType', fallback='NTLM')]
        self.stream_timeout = conf.getint('EWS', 'StreamTimeout', fallback=1)
        self.tmp_path = CFG().get_tmp_path()
        version = conf.get('EWS', 'Version')
        # 'xx.x.xxxx.x' -> (xx, x, xxxx, x)
        self.version = Version(build=Build(*tuple(map(int, version.split('.'))))) if version else None

    def _get_account(self):
        self.log.info('Connect to {} requested'.format(self.server))
        if self.cached_endpoint:
            # using cached values
            config = Configuration(service_endpoint=self.cached_endpoint,
                                   credentials=self.credentials,
                                   version=self.version,
                                   auth_type=self.cached_auth_type)
            account = Account(primary_smtp_address=self.cached_smtp_address,
                              config=config,
                              autodiscover=False,
                              access_type=DELEGATE)
        else:
            self.log.info('Connecting to {}'.format(self.server))
            try:
                with open('./etc/pass.txt') as f:
                    password = f.read()
            except:
                self.log.info('password read error, requesting for manual input')
                password = getpass(prompt='Enter your password, please:')

            if password:
                self.log.info('password acquired')
            else:
                self.log.warning('password is not set or empty')

            self.credentials = ServiceAccount(username='{}\\{}'.format(self.domain, self.username), password=password)
            config = Configuration(server=self.server, credentials=self.credentials,
                                   version=self.version, auth_type=self.auth_type)
            account = Account(primary_smtp_address=self.smtp_address, config=config,
                              autodiscover=False, access_type=DELEGATE)
            self.log.info('connected to {}'.format(self.server))
            # caching
            self.cached_endpoint = account.protocol.service_endpoint
            self.cached_auth_type = account.protocol.auth_type
            self.cached_smtp_address = account.primary_smtp_address

        self.is_connected = True

        return account

    @property
    def is_connected(self):
        return self._connected

    @is_connected.setter
    def is_connected(self, connected):
        self._connected = connected


    def exec_stream(self, account=None, force=False):
        """ force - True to create a new subscription """
        account = account or self._get_account()
        try:
            self.subscription_id = self.start_stream(account=account, force=force)
            if self.subscription_id:
                self.log.info('New Subscription Id: {}'.format(self.subscription_id))
                try:
                    return self.get_stream_events(account=account)
                except Exception:
                    self.log.error('Stream Get Events Error')
                    raise
        except KeyboardInterrupt:
            self.close()
            sys.exit('Terminated by user')
        except Exception as e:
            self.log.error('Unexpected exception while managing a stream {}'.format(e))
            self.close()
            raise

    def start_stream(self, account=None, force=False):
        """ force - True to create a new subscription """
        if not self.subscription_id or force:
            account = account or self._get_account()
            self.log.info('Attempting to create a Stream Subscription')
            return SubscribeStream(account).call(['inbox'], ['NewMailEvent'])
        else:
            self.log.info('Subscription already exists')
            return self.subscription_id

    def get_stream_events(self, account=None):
        if self.subscription_id:
            account = account or self._get_account()
            self.log.info('Starting to Get Events for {} Subscription'.format(self.subscription_id))
            return GetStreamingEvents(account).call([self.subscription_id], self.stream_timeout)
        self.log.info('No subscription to get events from')

    def stop_stream(self, account=None):
        if self.subscription_id:
            account = account or self._get_account()
            self.log.info('Attempting to unsubscribe from {}'.format(self.subscription_id))
            try:
                return UnsubscribeStream(account).call(self.subscription_id)
            except ErrorSubscriptionNotFound:
                self.log.warning('False unsubscribe attempt from {}'.format(self.subscription_id))
        self.subscription_id = None # occasionally causes ErrorSubscriptionNotFound
        self.log.info('No subscription to unsubscribe')
        return None

    def send_message(self, body, tag=None, to=None, is_reply=False):
        """ body: dictionary containing at least {'cmd': 'cmd', 'args': args}
            tag: reply tag, should be same as in the original message,
                  reply attachments should reside in tmp/reply folder
            to: recipient address
            is_reply: attachments should be searched in the reply tmp folder for replies
            returns: sent batch tag or None in case of failure
            TODO: calculate part and parts here, send messages according to result
        """
        recipient = to or self.to_address
        self.log.info('Preparing message to {}'.format(recipient))
        encoded = b64encode(json.dumps(body).encode('utf-8'))
        content = b'###' + encoded + b'###' # artifical delimiter
        # send message(s) based on the configured attachment policies
        batch_tag = tag or get_new_tag()
        attachments_folder = get_tmp(self.tmp_path, batch_tag, is_reply=is_reply)
        conf = CFG().conf
        max_files = conf.getint('Attachment', 'MaxFiles', fallback=1)
        max_file_size = conf.getint('Attachment', 'MaxFileSize', fallback=1024 * 1024 * 4)
        assert max_files > 0 and max_file_size >= 1024 * 100 # ~ 100KB, arbitrary plausible size
        # split attachment if necessary
        try:
            attachments = split_file(attachments_folder, max_file_size=max_file_size)
        except Exception as e:
            self.log.error('Batch {} attachments split error {}'.format(batch_tag, e))
            raise
        # all attachment should be sent with the minimal number of messages, one message should be sent anyway
        parts = -(-len(attachments) // max_files) or 1
        for part in range(parts):
            # parts counting starts from 1, not 0
            part_num = part + 1
            # TODO: set TextBody BodyTypeType to Text
            m = Message(account=self._get_account(),
                        subject=get_subject(self.local_prefix, part=part_num, parts=parts, tag=batch_tag)[1],
                        body=content,
                        to_recipients=[Mailbox(email_address=recipient)])
            # add current part's attachment(s) if there is any in tmp folder under the batch's tag
            for name, path in attachments[part:part + max_files]:
                self.log.info('Adding attachment {}, part {} out of {}'.format(path, part_num, parts))
                with open(path, mode='rb') as f:
                    binary_content = f.read()
                attachment = FileAttachment(name=name, content=binary_content)
                m.attach(attachment)
                self.log.debug('Attachment {}, part {} out of {} added'.format(path, part_num, parts))

            self.log.info('Sending message to {}, part {} out of {}'.format(recipient, part_num, parts))
            m.send()
            self.log.info('Message sent to {}, part {} out of {}'.format(recipient, part_num, parts))
        return batch_tag

    def receive_message(self, message, tag=None, once=False):
        """ message: tuple (message_id, change_key)
            tag: restrict message processing to the specified tag
            once: stop stream listening after batch complete
            returns: dictionary {'id': 'message_id', 'sender': 'a@b.c', 'tag': 'batch_tag',
                                 'body': {'cmd': 'cmd', 'args': args, 'refs': refs},
                                 'attachment': (name, path)}
        """
        def is_batch_complete(parts, folder):
            """ Batch parts could be received unordered,
                but the number of files in the named after
                the tag folder should tell us truth.
                Single part batches are always complete though.
            """
            if (parts == 1) or (len(get_files(folder)) == parts):
                return True
            return False

        received = {'id': '', 'sender': '', 'tag': tag,
                    'body': {'cmd': 'noop', 'args': 'NOOP', 'refs': {}},
                    'attachment': tuple()}
        if isinstance(message, tuple):
            # looks like message, processing
            self.log.info('receiving message {}'.format(message[0]))
            account = self._get_account()
            items = list(GetItem(account).call([message], [], IdOnly))
            try:
                m = Message.from_xml(items[0], account)
                # TODO: get subject in GetItem to check it before refresh
                # (SimpleField workaround required) or just get only the necessary fields
                m.refresh()
            except AttributeError:
                # TODO: handle erroneous object before passing it to Message class
                msg = '{} is not a message'.format(items[0])
                self.log.warning(msg)
                received.update({'body': {'cmd': 'noop', 'args': msg, 'refs': {}}})
            except ErrorItemNotFound:
                msg = 'Item {} not found'.format(message[0])
                self.log.warning(msg)
                received.update({'body': {'cmd': 'noop', 'args': msg, 'refs': {}}})
            else:
                m_id = m.item_id
                subject = m.subject
                prefix, batch_tag, part, parts = parse_subject(subject)
                sender = m.sender.email_address
                received.update({'id': m_id, 'sender': sender, 'tag': batch_tag})
                # tag restricts emails to the specific batch, prefix - to the specific sender
                # replies should not be replied, self and unknown messages should be ignored
                if prefix == self.remote_prefix and (batch_tag == tag or not tag):
                    # removing artifical delimiter ###, b' and ' since body is a string
                    raw_body = re.search(r"b'###(.+?)###'", m.text_body).group(1) # body or text_body?
                    try:
                        body = json.loads(b64decode(raw_body).decode('utf-8'))
                    except (UnicodeDecodeError, TypeError, BinAsciiError):
                        self.log.error('Message {} is malformed'.format(m_id))
                        # it seems that message is bogus or corrupted
                        received.update({'body': {'cmd': 'error', 'args': 'Malformed content', 'refs': {}}})
                    else:
                        self.log.info('New command <{}>, message {}'.format(body, m_id))
                        # attachments folder path, create only if there is anything to save
                        folder = get_tmp(self.tmp_path, batch_tag, is_reply=tag, create=bool(m.attachments))
                        # attachments should be saved even if the
                        # received command is unsupported (yet?).
                        # We don't want to waste our precious
                        # communication channel capacity for duplicates, do we?
                        # In that case command sender should handle them manually though.
                        for attachment in m.attachments:
                            if isinstance(attachment, FileAttachment):
                                fullpath = os.path.join(folder, attachment.name)
                                with open(fullpath, 'wb') as f:
                                    f.write(attachment.content)
                            self.log.info('Attachment {} saved'.format(fullpath))
                        m.delete()
                        self.log.info('Original message {} deleted'.format(m_id))

                        # only complete message chains should be processed further
                        if is_batch_complete(parts, folder):
                            self.log.info('Batch {} is complete'.format(batch_tag))
                            if once:
                                self.stop_stream(account)
                            # merging attachments if any
                            try:
                                merged_attachment = merge_files(folder)
                            except Exception as e:
                                self.log.error('Batch {} attachments merge failed {}'.format(batch_tag, e))
                                raise
                            # theoretically the received bundle could be applied
                            # at this stage, but we don't know active git executor class
                            # and applying on the nodes side could give us more flexibility
                            # e.g. unsupported operations could be ignored
                            received.update({'body': body, 'attachment': merged_attachment})
                        else:
                            self.log.info('Message {}, batch {}, part {} of {}'.format(m_id, batch_tag, part, parts))
                            progress = round(part / parts * 100, 2)

                            received.update({'body':  {'cmd': 'partial',
                                                       'args': 'Progress: {}%'.format(progress),
                                                       'refs': {}}})
                else:
                    self.log.info('Message {} ignored'.format(m_id))
        else:
            self.log.info('Not a message: {}'.format(message))

        return received

    def close(self):
        if self.is_connected:
            # TODO: implement without unnecessary account login (even cached)
            account = self._get_account()
            self.stop_stream(account)
            account.protocol.close()
            self.log.info('EWS closed')
        else:
            self.log.info('Not connected EWS closed')

