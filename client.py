#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created: 2017-12-05

@author: pymancer

Local to remote Git over EWS synchronizer.
"""
import cmd
import os.path

from ews import EWS
from logger import Log
from config import CFG
from gitlib import CLIENT
from helpers import get_repo_name, get_new_tag


class GitEmulator(cmd.Cmd):
    """ Git command line (limited support) shell emulator.
        Message body dictionary should contain at least 'command' and 'args' keys.
        TODO: autocomplete, filter out multi commands, add stream interrupt without exit
    """
    intro = ('Type `?` to list commands, `q` to quit.\n'
             '`? command` to get help about the command usage.\n')
    prompt = '(mgit)> '

    def __init__(self, *args, **kwargs):
        self.log = Log(self.__class__.__name__.lower()).get()
        self.log.info('Git `remote` shell init')
        self.git = CLIENT()
        self.ews = EWS()
        super().__init__(*args, **kwargs)

    def _parse(self, arg):
        return arg.split()

    def _is_repo(self):
        if self.git.repo:
            return True
        self._show_result(False, {'cmd': 'error', 'args': '{} is not a Git repo'.format(self.git.repo_path)})
        return False

    def _show_result(self, success, body):
        """ User notification.
            success: boolean, formats output as error if False
            body: dictionary with 'args' key to show as a message
        """
        msg = str(body['args'])
        if success:
            print(msg)
        else:
            delim = (min(70, len(msg)) // 2) * '='
            self.log.debug('{}'.format(msg))
            print('{delim}ERROR{delim}\n{}\n{delim}ERROR{delim}'.format(msg, delim=delim))

    def _process_command(self, body, create_cb=None, apply_cb=None):
        """ param: body: dictionary of the Git command parts from the message body
            param: create_cb: callback to handle bundle creation instead of the default one
            param: apply_cb: callback to handle bundle applying instead of the default one
        """
        create_bundle = create_cb or self.git.create_bundle
        apply_bundle = apply_cb or self.git.apply_bundle
        # new batch tag
        tag = get_new_tag()
        # prepare local changes from tag for server, passing body as it is easier to use
        created, msg = create_bundle(tag, args=body)
        self._show_result(created, msg)

        if created:
            # bundle created or not necessary
            tag = self.ews.send_message(body, tag=tag)
            # waiting for server reply
            # forcing new subscription due subscription ending on each complete batch
            stream = self.ews.exec_stream(force=True)
            # remote response status
            completed = False
            # waiting for batch complete or connection end
            for message in stream:
                # notify user on execution status
                completed = self._track_progress(self.ews.receive_message(message, tag=tag, once=True))
                if completed:
                    break
            if completed:
                # apply received bundle and notify user, passing body as it is easier to use
                applied, msg = apply_bundle(tag, args=body)
                self._show_result(applied, msg)
                if applied:
                    # save tag as a next local bundle starting point
                    self.git.set_tag(tag, conf=True)
            else:
                self._show_result(False, {'cmd': 'error', 'args': 'Stream ended before batch {} completed'.format(tag)})

    def _track_progress(self, received):
        """ Handles server side progress notifications. """
        cmd = received['body']['cmd']
        msg = received['body']['args']
        batch_complete = False if cmd in ('noop', 'partial') else True
        success = False if cmd == 'error' else True
        self._show_result(success, {'cmd': cmd, 'args': msg})
        return batch_complete

    def do_git(self, arg):
        """ Redundant syntax. Use `git` command arguments only. """
        _cmd = 'git'

        self.log.debug('{} {}'.format(_cmd, arg))

        try:
            args = self._parse(arg)
            return getattr(self, 'do_' + args[0])(' '.join(args[1:]))
        except IndexError:
            self._show_result(False, {'cmd': _cmd, 'args': 'No command received.'})
        except AttributeError:
            self._show_result(False, {'cmd': _cmd, 'args': '`{}` command is not supported'.format(args[0])})

    def do_status(self, arg):
        """ Show the working tree status """
        if self._is_repo():
            self._show_result(*self.git.status(self._parse(arg)))

    def do_clone(self, arg):
        """ Create new repo from the existing one.
            example call: `clone -b branchname git@gitlab.com:authorlogin/project-name.git`
            Remote repo URL must be the last argument.
        """
        def create_cb(tag, args=None):
            return (True, {'cmd': 'success', 'args': 'No repo to sync'})

        def apply_cb(tag, args=None):
            """ Cloned repo should use a new bundle as an origin """
            return self.git.apply_bundle_bare(tag, args=args)

        _cmd = 'clone'

        # preliminary error checks
        cfg = CFG()
        args = self._parse(arg)
        # TODO: implement native clone arguments handling
        try:
            # branch name should be next to the '-b' argument if any
            branch_idx = args.index('-b') + 1
        except ValueError:
            # default branch will be used
            branch_idx = None

        try:
            # repo url must be the last argument
            url = args[-1]
            # check if repo url is plausible
            assert get_repo_name(url)
            # check if branch is actually provided
            branch = args[branch_idx] if branch_idx else cfg.conf.get('Repo', 'DefaultBranch', fallback='master')
        except (IndexError, AssertionError):
            self._show_result(False, {'cmd': 'error', 'args': 'Bad command. Type `? {}` for help'.format(_cmd)})
        else:
            repo_path = self.git.repo_path
            if self.git.repo:
                self._show_result(False, {'cmd': 'error', 'args': '{} is Git repo, cannot {}'.format(repo_path, _cmd)})
            elif os.path.exists(repo_path):
                msg = {'cmd': 'error', 'args': 'Directory {} must not exist to {}'.format(repo_path, _cmd)}
                self._show_result(False, msg)
            else:
                bases = cfg.get_bases()
                if bases:
                    # clone should initiate a new synchronization cycle
                    cfg.set_tag()
                    self.log.warning('bases {} removed from the config file'.format(bases))

                msg_body = {'cmd': _cmd, 'args': args, 'branch': branch, 'url': url}
                self.log.info('{} from {} '.format(_cmd, url))
                args.insert(0, _cmd)
                self._process_command(msg_body, create_cb=create_cb, apply_cb=apply_cb)

    def do_push(self, arg):
        """ Update remote refs along with associated objects
            example call: push
        """
        _cmd = 'push'

        if self._is_repo():
            self.log.info('{} {}'.format(_cmd, arg))
            args = self._parse(arg)
            self._process_command({'cmd': _cmd, 'args': args})

    def do_pull(self, arg):
        """ Fetch from and integrate with another repo or a local branch
            example call: pull
        """
        def create_cb(tag, args=None):
            """ Changes starting from the last bundle should be sent. """
            if not tag or self.git.repo.head.commit.diff(CFG().get_tag(self.git.get_active_branch())):
                return self.git.create_bundle(tag, args=args)
            return (True, {'cmd': 'success', 'args': '{} without local changes or untracked repo'.format(_cmd)})

        def is_clean():
            """ Checks if working tree is clean, i.e. pullable without conflicts """
            if self.git.repo.is_dirty():
                self._show_result(False, {'cmd': 'error', 'args': '`stash` or `commit` changed files first'})
                return False
            return True

        _cmd = 'pull'

        if self._is_repo() and is_clean():
            self.log.info('{} {}'.format(_cmd, arg))
            args = self._parse(arg)
            self._process_command({'cmd': _cmd, 'args': args}, create_cb=create_cb)

    def do_fetch(self, arg):
        """ Download objects and refs from another repository
            example call: fetch
        """
        def create_cb(tag, args=None):
            """ Local changes will not be sent. """
            return (True, {'cmd': 'success', 'args': 'No need to sync local changes before {}'.format(_cmd)})

        _cmd = 'fetch'

        if self._is_repo():
            self.log.info('{} {}'.format(_cmd, arg))
            args = self._parse(arg)
            self._process_command({'cmd': _cmd, 'args': args}, create_cb=create_cb)

    def do_q(self, __):
        """ Close the mgit session """
        self.log.info('Git emulator exit')
        self.ews.close()
        return True

if __name__ == '__main__':
    GitEmulator().cmdloop()

